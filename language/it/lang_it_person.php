<?php
$LDPerson='Persona';
$LDPatient='Paziente';
$LDBloodGroup='Gruppo sanguigno';
# these are blood groups
$LDA='A';
$LDB='B';
$LDAB='AB';
$LDO='O';
# 2003-08-28 EL
$LDPlsEnterLastName='Inserire il cognome';
$LDPlsEnterFirstName='Inserire il nome';
$LDPlsEnterDateBirth='Inserire la data di nascita';
$LDPlsSelectSex='Inserire il sesso';
$LDPlsEnterStreetName='Inserire l\'indirizzo';
$LDPlsEnterBldgNr='Inserire il numero civico';
#2003-10-03 EL
$LDPlsEnterZip='Inserire il CAP';
$LDPlsEnterFullName='Inserire il proprio nome e cognome';
/* begin SALVO 23/11/07 */
$LDPlsEnterCityTown='Inserire la Citt�';
$LDPlsEnterSss_nr='Inserire il Codice Fiscale';
/* end SALVO 23/11/07 */
?>